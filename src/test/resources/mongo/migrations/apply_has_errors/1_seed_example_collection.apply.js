var example_collection =
  [
    {
      "name": "foo",
    },
    {
      "name": "foo",
    },
    {
      "name": "foobar",
    },
  ];
db.example_collection.insert(example_collection);

db.example_collection.createIndex( { "name": 1 }, { unique: true } );
