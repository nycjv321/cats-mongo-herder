var example_collection =
  [
    {
      "name": "foo",
    },
    {
      "name": "bar",
    },
    {
      "name": "foobar",
    },
  ];
db.example_collection.insert(example_collection);

db.example_collection.createIndex( { "name": 1 }, { unique: true } );
