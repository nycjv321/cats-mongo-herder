package com.nycjv321.cats.mongo

import cats.effect.Sync
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.{MongoClient, MongoCollection}

class Examples[F[_]: Sync](mongoClient: MongoClient) {
  val codecRegistry = fromRegistries(CodecRegistries.fromCodecs(new MD5Codec()), fromProviders(classOf[Example]), DEFAULT_CODEC_REGISTRY)


  val examples: MongoCollection[Example] = mongoClient
    .getDatabase("test")
    .withCodecRegistry(codecRegistry)
    .getCollection("example_collection")

  def drop() : F[Unit] = {
    examples.drop().toF()
  }

  def list(): F[Seq[Example]] = {
    examples.find().toF()
  }
}
