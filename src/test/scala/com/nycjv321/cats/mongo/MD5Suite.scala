package com.nycjv321.cats.mongo

import java.io.File

import cats.effect.IO

class MD5Suite  extends munit.FunSuite {
  test("can calculate md5 hashes") {
    val md5 = MD5.calculate(new File(getClass.getResource("/mongo/migrations/2_seed_example_collection_some_more.apply.js").getFile))
    assertEquals(md5, MD5("468cbd73a9b859a654149a5774cd0ff6"))

  }
}
