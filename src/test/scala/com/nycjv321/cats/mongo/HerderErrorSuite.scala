package com.nycjv321.cats.mongo

import java.io.File

class HerderErrorSuite extends munit.FunSuite {

  test("NoMigrationsFound#toString") {
    val str = NoMigrationsFound("some_path").toString()
    assert(str == "no migrations found at some_path")
  }

  test("MigrationFailed#toString") {
    val str = MigrationFailed(new File("text.js"), "error message").toString()
    assert(str == s"got an error running migration located at text.js, see:\nerror message")
  }

}
