package com.nycjv321.cats.mongo

import java.io.File

import cats.data.Chain
import cats.data.Validated.{Invalid, Valid}
import com.nycjv321.cats.mongo.MigrationsFileValidator._


class MigrationsFileValidatorSuite extends munit.FunSuite {

  test("validates properly generated migrations") {
    val validatedMigrations: ValidationResult[PartialMigration[File]] = validateMigrations(
      new File("20_remove_unique_index_from_example.apply.js") ::
        new File("20_remove_unique_index_from_example.unapply.js") ::
        Nil
    )

    validatedMigrations match {
      case Valid(unApplyMigration :: applyMigration :: Nil) => {
        assert(applyMigration.name == "remove_unique_index_from_example")
        assert(applyMigration.version == 20)
        assert(applyMigration.unApplyAsOption.isEmpty)
        assert(unApplyMigration.name == "remove_unique_index_from_example")
        assert(unApplyMigration.version == 20)
        assert(unApplyMigration.applyAsOption.isEmpty)

      }
      case Invalid(e) => {
        fail("did not expect to receive validation errors")
      }
    }

  }

  // todo make this data driven
  test("that a list of versions properly incremented are validated") {
    def testVersionIncremented(versions: VersionsPair, expectedValid: Int): Unit = {
      VersionsPair.validate(versions) match {
        case Valid(head) =>  assert(head == expectedValid)
        case Invalid(e) => fail(e.toString)
      }
    }

    testVersionIncremented(NoVersions, 0)
    testVersionIncremented(FirstVersions, 1)
    testVersionIncremented(MultipleVersions(2, 1), 2)
    testVersionIncremented(MultipleVersions(3, 2), 3)
    testVersionIncremented(MultipleVersions(5000, 4999), 5000)

  }

  test("invalid versions report errors") {
    def testVersionIncremented(versions: VersionsPair, expectedError: String): Unit = {
      VersionsPair.validate(versions) match {
        case Valid(head) =>          fail(s"did not expect to pass validation $head")

        case Invalid(e) => assert(e.toString == expectedError)
      }
    }

    testVersionIncremented(MultipleVersions(3, 3), "version was not properly incremented: 3 -> 3")
    testVersionIncremented(MultipleVersions(43, 23), "version was not properly incremented: 23 -> 43")

  }



  test("returns validation errors for malformed migrations") {
    val validatedMigrations: ValidationResult[PartialMigration[File]] = validateMigrations(
      new File("20_remove_unique_index_from_example.baz.js") ::
        new File("20_remove_unique_index_from_example.apply.js") ::
        new File("20_remove_unique_index_from_example.foo.js") ::
        Nil
    )

    validatedMigrations match {
      case Valid(_) => {
        fail("did not expect to pass validation")
      }
      case Invalid(e) => {
        e.uncons match {
          case (UnableToDetermineMode(firstError), Chain(UnableToDetermineMode(secondError))) => {
            assert(firstError == "20_remove_unique_index_from_example.baz.js")
            assert(secondError == "20_remove_unique_index_from_example.foo.js")
          }
        }
      }
    }
  }

  test("returns validation error when there are no migrations") {
    val validatedMigrations: ValidationResult[PartialMigration[File]] = validateMigrations(Nil)
    validatedMigrations match {
      case Valid(files) =>
        assert(files.isEmpty)
      case Invalid(_) =>
        fail("did not expect to fail validation")
    }
  }

  test("returns validation errors when migrations are partially defined") {
    validateMergedMigrations(
      PartialMigration[File](version = 0, name = "missingApply", Some(new File("")), None) ::
        PartialMigration[File](version = 1, name = "missingUnApply", Some(new File("")), None) ::
        Nil
    ) match {
      case Valid(_) =>
        fail("did not expect to pass validation")
      case Invalid(e) =>
        e match {
          case Chain(UnexpectedNumberOfMigrations(1, "missingUnApply", 1), UnexpectedNumberOfMigrations(0, "missingApply", 1)) => ()
          case other => fail(s"unexpected number of errors returned: ${other.length}")
        }
    }

  }

  //  test("EitherS") {
  //    sealed trait SequencedEither[+E, +A] extends Product with Serializable {
  //
  //      def pure[B](a: B): SequencedEither[E, B] = {
  //        Rights[E, B](a :: Nil)
  //      }
  //
  //      def map[B](f: A => B): SequencedEither[E, B] = {
  //        toEither match {
  //          case Right(values) => Rights(values.map(f))
  //          case _ => Empty()
  //        }
  //      }
  //
  //      def flatMap[E, B](f: A => SequencedEither[E, B]): SequencedEither[E, B] = {
  //
  //        def inverse(seq: Seq[Rights[E, B]]) : Seq[B] = {
  //          (seq) match {
  //            case (Rights(value) :: tail) => inverse(tail) ++ value
  //            case (Nil) => Seq.empty
  //          }
  //        }
  //
  //        toEither match {
  //          case Right(values) => values.map(f) match {
  //            case seq: Seq[Rights[E, B]] => Rights(inverse(seq))
  //          }
  ////          case Left(errors) => Lefts(errors)
  //        }
  //      }
  //
  //      def unzip: (Seq[E], Seq[A]) = {
  //        this match {
  //          case Empty() => {
  //            (Seq.empty, Seq.empty)
  //          }
  //          case Lefts(errors) => {
  //            (errors, Seq.empty)
  //          }
  //          case Rights(values) => {
  //            (Seq.empty, values)
  //          }
  //          case Both(errors, values) => {
  //            (errors, values)
  //          }
  //        }
  //      }
  //
  //      def toEither: Either[Seq[E], Seq[A]] = {
  //        this match {
  //          case Empty() => {
  //            Right(Seq.empty)
  //          }
  //          case Lefts(errors) => {
  //            Left(errors)
  //          }
  //          case Rights(values) => {
  //            Right(values)
  //          }
  //          case Both(errors, _) => {
  //            Left(errors)
  //          }
  //        }
  //      }
  //
  //
  //    }
  //    case class Empty[+E, +A]() extends SequencedEither[E, A]
  //    case class Both[+E, +A](lefts: Seq[E] = Seq.empty, rights: Seq[A] = Seq.empty) extends SequencedEither[E, A]
  //    case class Lefts[+E, +A](lefts: Seq[E] = Seq.empty) extends SequencedEither[E, Nothing]
  //    case class Rights[+E, +A](rights: Seq[A] = Seq.empty) extends SequencedEither[Nothing, A]
  //
  //    object SequencedEither {
  //      def fromEithers[E, A](a: Seq[Either[E, A]]): SequencedEither[E, A] = {
  //        val seed: SequencedEither[E, A] = Empty[E, A]()
  //        a.foldLeft(seed) {
  //          case (Empty(), Left(error)) => Lefts(error :: Nil)
  //          case (Empty(), Right(value)) => Rights(value :: Nil)
  //          case (Rights(rights), Left(error)) => Both(error :: Nil, rights)
  //          case (Lefts(errors), Right(value)) => Both(errors, value :: Nil)
  //          case (Rights(values), Right(value)) => Rights(values :+ value)
  //          case (Lefts(errors), Left(error)) => Both(errors :+ error)
  //          case (Both(lefts, rights), Left(error)) => Both(lefts :+ error, rights)
  //          case (Both(lefts, rights), Right(value)) => Both(lefts, rights :+ value)
  //        }
  //      }
  //
  //    }
  //
  //
  //
  //    val seqOfEithers: Seq[Either[Int, String]] = Right("foo") :: Left(0) :: Nil
  //    val result: SequencedEither[Int, String] = SequencedEither.fromEithers(seqOfEithers)
  //
  //
  //    for {
  //      a <- result
  //      b <- result
  //    } yield {
  //      a + b
  //    }
  //
  //
  //  }

}
