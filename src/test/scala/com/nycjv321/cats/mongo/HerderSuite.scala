package com.nycjv321.cats.mongo

import cats.effect.IO
import com.nycjv321.cats.mongo.MigrationStatus.{Failed, Passed}
import org.mongodb.scala.MongoClient

import scala.util.{Failure, Success, Try}

case class Example(name: String)

class HerderSuite extends munit.FunSuite {

  private val mongoClient: MongoClient = MongoClient()
  val migrations: Migrations[IO] = Migrations.impl[IO](mongoClient)
  val examples: Examples[IO] = new Examples[IO](mongoClient)
  val migrationsExecutor: MigrationExecutor[IO] = MigrationExecutor.impl[IO]()

  override def beforeEach(context: BeforeEach): Unit = {
    migrations.reset().unsafeRunSync()
    mongoClient.getDatabase("test").getCollection("example_collection").drop().toF[IO]().unsafeRunSync()
  }

  def runMigrationApp(path: String): IO[Seq[HerderError]] = {
    val herder = Herder.impl[IO](manager = migrations, executor = migrationsExecutor)
    herder.runUsing(HerderConfig(path = path))
  }

  def runTest(path: String, filter: AppliedMigration => Boolean, expectedStatus: MigrationStatus, expectedMigrationSize: Int = 0)(testErrors: scala.Seq[HerderError] => Unit): Unit = {
    (for {
      errors <- runMigrationApp(path)
      migrations <- migrations.list()
      examples <- examples.list()
    } yield {
      val executedMigration = migrations.find(filter)
      assert(executedMigration.isDefined)
      executedMigration.foreach { migration =>
        assert(migration.isMarkedAs(expectedStatus))
      }
      assert(examples.size == expectedMigrationSize)
      // improve logging to include more detailed messaging
      testErrors(errors)
    }).unsafeRunSync()
  }

  test("can run migrations") {
    runTest(
      path = "/mongo/migrations/can_run_migrations",
      filter = migration => migration.version == 1 && migration.name == "seed_example_collection",
      expectedStatus = Passed,
      expectedMigrationSize = 3
    ) { errors =>
      assert(errors.isEmpty)
    }
  }

  implicit class AssertableMigration(migration: AppliedMigration) {

    def assertStatus(migrationStatus: MigrationStatus): Unit = {
      assert(migration.status == migrationStatus)
    }
  }

  implicit class AssertableMigrations(migrations: scala.Seq[AppliedMigration]) {

    def assertThat(version: Int, name: String)(f: AppliedMigration => Unit): Unit = {
      val migrationOpt = migrations.find {
        migration => migration.version == version && migration.name == name
      }
      assert(migrationOpt.isDefined)
      migrationOpt.foreach(f)
    }

  }

  test("can run multiple migrations") {
    runTest(
      path = "/mongo/migrations/can_run_multiple_migrations",
      filter = migration => migration.version == 1 && migration.name == "seed_example_collection",
      expectedStatus = Passed,
      expectedMigrationSize = 6
    ) { errors =>
      assert(errors.isEmpty)
    }
  }


  test("run migrations unsafely") {
    Herder.runUnSafe(manager = migrations, executor = migrationsExecutor, herderConfig = HerderConfig(path = "/mongo/migrations/can_run_migrations"))

    (for {
      migrations <- migrations.list()
      examples <- examples.list()
    } yield {
      val executedMigration = migrations.find {
        migration => migration.version == 1 && migration.name == "seed_example_collection"
      }
      assert(executedMigration.isDefined)
      executedMigration.foreach { migration =>
        assert(migration.isMarkedAs(Passed))
      }
      assert(examples.size == 3)
      // improve logging to include more detailed messaging
    }).unsafeRunSync()
  }

  test("can run the same migrations over and over") {
    Try(Herder.runUnSafe(
      manager = migrations,
      executor = migrationsExecutor,
      herderConfig = HerderConfig(path = "/mongo/migrations/can_run_migrations"))
    ) match {
      case Failure(e) => fail(s"unexpected for execution to fail, see $e")
      case Success(_) => ()
    }
    Try(Herder.runUnSafe(
      manager = migrations,
      executor = migrationsExecutor,
      herderConfig = HerderConfig(path = "/mongo/migrations/can_run_migrations"))
    ) match {
      case Failure(e) => fail(s"unexpected for execution to fail, see $e")
      case Success(a) => ()
    }
  }

  test("incorrect migrations are properly reported when ran unsafely") {
    Try(Herder.runUnSafe(
      manager = migrations,
      executor = migrationsExecutor,
      herderConfig = HerderConfig(path = "/mongo/migrations/apply_has_errors"))
    ) match {
      case Failure(e) => assert(e.getMessage.nonEmpty)
      case Success(_) => fail("unexpected for execution to fail")
    }

    (for {
      migrations <- migrations.list()
      examples <- examples.list()
    } yield {
      val executedMigration = migrations.find {
        migration => migration.version == 1 && migration.name == "seed_example_collection"
      }
      assert(executedMigration.isDefined)
      executedMigration.foreach { migration =>
        assert(migration.isMarkedAs(Failed))
      }
      assert(examples.isEmpty)
    }).unsafeRunSync()
  }

  test("incorrect migrations are properly reported") {
    runTest(
      path = "/mongo/migrations/apply_has_errors",
      filter = migration => migration.version == 1 && migration.name == "seed_example_collection",
      expectedStatus = Failed
    ) { errors =>
      assert(errors.size == 1)
      errors match {
        case MigrationFailed(f, e) :: Nil => {
          assert(f.getName == "1_seed_example_collection.apply.js")
          assert(e.nonEmpty) // this indicates the mongo log was captured
        }
        case _ => fail("unexpected number of errors returned")
      }
    }
  }

  test("errors are returned when already ran migrations md5s don't match") {
    Herder.runUnSafe(
      manager = migrations,
      executor = migrationsExecutor,
      herderConfig = HerderConfig(path = "/mongo/migrations/can_run_migrations")
    )

    (for {
      migrations <- migrations.list()
      migrationOpt = migrations.find {
        migration => migration.version == 1 && migration.name == "seed_example_collection"
      }
      _ <- migrationOpt.map(m => this.migrations.update(m.copy(md5 = MD5("foo")))).getOrElse(fail("unable to find migration"))
      errors <- runMigrationApp("/mongo/migrations/apply_has_errors")
    } yield {
      errors match {
        case InvalidMD5Error(MD5(current), MD5(previous)) :: Nil =>
          assert(current == "912dd8cd4caa933ebc0ce7f2b1ef3820")
          assert(previous == "65ebe096dea1d48bba74614c9ee7883d")
        case _ => fail("unvalid number of errors returned")
      }
      assert(errors.nonEmpty)
    }).unsafeRunSync()

  }

}
