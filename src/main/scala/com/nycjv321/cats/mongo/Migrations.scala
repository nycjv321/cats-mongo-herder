package com.nycjv321.cats.mongo

import cats.effect.{IO, Sync}
import cats.implicits._
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.{DEFAULT_CODEC_REGISTRY, Macros}
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.IndexOptions
import org.mongodb.scala.model.Indexes._
import org.mongodb.scala.model.Updates._


trait Migrations[F[_]] {
  def update(appliedMigration: AppliedMigration): F[Unit]

  def reset(): F[Unit]

  def list(): F[Seq[AppliedMigration]]

  def findByVersionAndName(version: Int, name: String): F[Option[AppliedMigration]]

  def add(version: Version, name: String, md5: MD5, migrationStatus: MigrationStatus): F[AppliedMigration]
}




object Migrations {

  def impl[F[_] : Sync](mongoClient: MongoClient): Migrations[F] = {
    val codecRegistry = fromRegistries(fromProviders(new MigrationStatusCodecProvider), CodecRegistries.fromCodecs(new MD5Codec()), fromProviders(classOf[AppliedMigration]), DEFAULT_CODEC_REGISTRY)

      val database: MongoDatabase = mongoClient.getDatabase("migrations").withCodecRegistry(codecRegistry)

    val history: MongoCollection[AppliedMigration] = database.getCollection("history")

    // create the collection
    history.countDocuments().toF[IO]().unsafeRunSync()
    // create an index on it
    history
      .createIndex(
        compoundIndex(ascending("name"), descending("version")),
        IndexOptions().background(false).unique(true)
      ).toF[IO]()
      .unsafeRunSync()

    class MigrationsImpl[F[_] : Sync] extends Migrations[F] {
      override def reset(): F[Unit] = {
        history.drop().toF[F]()
      }

      override def list(): F[Seq[AppliedMigration]] = {
        history.find().toF[F]()
      }

      override def findByVersionAndName(version: Int, name: String): F[Option[AppliedMigration]] = {
        val filters: Bson = and(equal("version", version), equal("name", name))
        history.find(filters).toIOOption[F]()
      }

      override def add(version: Version, name: String, md5: MD5, migrationStatus: MigrationStatus): F[AppliedMigration] = {
        val appliedMigration = AppliedMigration(version = version, name = name, md5 = md5, status = migrationStatus)
        history.insertOne(appliedMigration).toF[F]().map(_ => appliedMigration)
      }

      override def update(appliedMigration: AppliedMigration): F[Unit] = {
        history.updateOne(equal("_id", appliedMigration.md5), set("md5", appliedMigration.md5.value)).toF[F]()
      }
    }
    new MigrationsImpl[F]()
  }
}
