package com.nycjv321.cats.mongo

import org.bson.codecs.Codec
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}

class MigrationStatusCodecProvider extends CodecProvider {
  override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
    if (isCaseObjectEnum(clazz)) {
      new MigrationStatusCodec().asInstanceOf[Codec[T]]
    } else {
      null
    }

  }

  private def isCaseObjectEnum[T](clazz: Class[T]): Boolean = {
    clazz.isInstance(MigrationStatus.Failed) || clazz.isInstance(MigrationStatus.Passed)
  }
}
