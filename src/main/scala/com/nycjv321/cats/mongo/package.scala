package com.nycjv321.cats

import java.io._
import java.security.{DigestInputStream, MessageDigest}

import cats.data.{NonEmptyChain, Validated, ValidatedNec}
import cats.data.Validated.Valid
import cats.effect.concurrent.Semaphore
import cats.effect.{ContextShift, IO, Resource, Sync}
import org.mongodb.scala.{Completed, FindObservable, SingleObservable}

import scala.concurrent.ExecutionContext
import cats.implicits._
import org.mongodb.scala.bson.ObjectId
import org.typelevel.log4cats.slf4j.Slf4jLogger
package object mongo {

  type Version = Int

  def digestInputStream(inputStream: FileInputStream, guard: Semaphore[IO]): Resource[IO, DigestInputStream] = {
    val md5 = MessageDigest.getInstance("MD5")
    Resource.make {
      IO(new DigestInputStream(inputStream, md5))
    } { inStream =>
      guard.withPermit {
        IO(inStream.close()).handleErrorWith(_ => IO.unit)
      }
    }
  }

  def inputStream(f: File, guard: Semaphore[IO]): Resource[IO, (FileInputStream)] =
    Resource.make {
      IO(new FileInputStream(f))
    } { inStream =>
      guard.withPermit {
        IO(inStream.close()).handleErrorWith(_ => IO.unit)
      }
    }

  sealed trait MigrationStatus
  object MigrationStatus {
    case object Failed extends MigrationStatus
    case object Passed extends MigrationStatus

  }

  sealed trait MigrationMode

  sealed trait HerderError

  case class InvalidMD5Error(current: MD5, previous: MD5) extends HerderError

  trait VersionsPair

  case class MigrationFailedException(file: File, message: String) extends RuntimeException(s"received error executing migration located at $file, see:\n $message")

  case class HerderException(herderException :NonEmptyChain[HerderError]) extends RuntimeException(herderException.toString)


  case class HerderConfig(path: String = "/mongo/migrations")

  case class MultipleVersions(currentVersion: Version, previousVersions: Version) extends VersionsPair

  case class InvalidVersion(value: Version) extends VersionsPair

  case class PartialMigration[F](version: Int, name: String, applyAsOption: Option[F] = None, unApplyAsOption: Option[F] = None)

  case class Migration[F](version: Version, name: String, applyMigration: F, unApplyMigration: F)

  case class NoMigrationsFound(path: String) extends HerderError {
    override def toString(): String = {
      "no migrations found at " + path
    }
  }
  case class MigrationFailed(file: File, message: String) extends HerderError {
    override def toString(): String = {
      "got an error running migration located at " + file + ", see:\n" + message
    }
  }


  case class UnexpectedNumberOfMigrations(version: Int, name: String, number: Int) extends HerderError {
    override def toString: String = {
      "got an unexpected number of migrations for (" + version.toString + ") " + name + ", found " + number.toString
    }

  }

  case class MissingMigration(version: Int, name: String, migrationMode: MigrationMode) extends HerderError

  case class UnableToDetermineMode(fileName: String) extends HerderError

  case class UnableToDetermineVersion(fileName: String) extends HerderError
  case class VersionIncrementError(currentVersion: Version, previousVersion: Version) extends HerderError {
    override def toString: String = {
      s"version was not properly incremented: $previousVersion -> $currentVersion"
    }
  }

  case class InvalidBaseVersion(v: Version) extends HerderError {
    override def  toString: String = {
      s"first migration did not start with 1, was $v"
    }
  }

  case class PreviousMigrationFailed(appliedMigration: AppliedMigration) extends HerderError

  case object FirstVersions extends VersionsPair

  case object NoVersions extends VersionsPair

  object VersionsPair {

    def validate(versions: VersionsPair): ValidatedNec[HerderError, Int] = {
      versions match {
        case MultipleVersions(head, middle) if head - middle == 1 => head.validNec[HerderError]
        case MultipleVersions(head, middle) if head - middle != 1 =>VersionIncrementError(head, middle).invalidNec[Int]
        case FirstVersions => Valid(1)
        case InvalidVersion(value) => InvalidBaseVersion(value).invalidNec[Int]
        case NoVersions => Valid(0)
      }
    }


    def getCurrentAndLastVersion(versions: Seq[Version]): VersionsPair = {
      versions.reverse match {
        case x :: xs :: _ => MultipleVersions(x, xs)
        case 1 :: Nil => FirstVersions
        case x :: Nil => InvalidVersion(x)
        case Nil => NoVersions
      }
    }

  }

  object ApplyMigration extends MigrationMode

  object UnapplyMigration extends MigrationMode

  implicit val contextShift: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  implicit class RichFindObservable[A](obv: FindObservable[A]) {
    def toIOOption[F[_] : Sync](): F[Option[A]] = {
      obv.toF[F]().flatMap {
        case Seq(one) => Sync[F].pure(Option(one))
        case Seq() => Sync[F].pure(None)
        case _ => Sync[F].raiseError(new IllegalStateException("multiple records returned for query"))
      }
    }

    def toF[F[_] : Sync](): F[Seq[A]] = {
      Sync[F].delay {
        IO.fromFuture(IO {
          obv.toFuture()
        }).unsafeRunSync()
      }
    }
  }

  implicit class RichSingleObservable[A](obv: SingleObservable[A]) {
    def toF[F[_] : Sync](): F[Unit] = {
      Sync[F].delay {
        IO.fromFuture(IO {
          obv.toFuture()
        }).unsafeRunSync()
        ()
      }
    }
  }

  object AppliedMigration {
    def apply(version: Int, name: String, md5: MD5, status: MigrationStatus): AppliedMigration =
      AppliedMigration(new ObjectId(), version, name, md5: MD5, status: MigrationStatus)
  }

  case class AppliedMigration(_id: ObjectId, version: Int, name: String, md5: MD5, status: MigrationStatus) {
    def isMarkedAs(migrationStatus: MigrationStatus): Boolean = this.status == migrationStatus
  }

//  implicit def unsafeLogger[F[_]: Sync] = Slf4jLogger.getLogger[F]

}
