package com.nycjv321.cats.mongo

import java.io.File

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNec
import cats.effect.{IO, Sync}
import cats.implicits._
import org.typelevel.log4cats.slf4j.Slf4jLogger


object Herder {

  def runUnSafe(manager: Migrations[IO], executor: MigrationExecutor[IO], herderConfig: HerderConfig = HerderConfig()): Unit = {
    val function: HerderConfig => IO[Seq[HerderError]] = impl[IO](manager, executor).runUsing _
    function(herderConfig).flatMap { errors =>
      for {
        logger <- Slf4jLogger.create[IO]
        errorString = errors.map(e => e.toString).mkString("\n")
        _ <- logger.error(errorString)
      } yield {
        if (errorString.nonEmpty) {
          throw new IllegalStateException(errorString)
        }
      }
    }.unsafeRunSync()
  }

  def impl[F[_] : Sync](manager: Migrations[F], executor: MigrationExecutor[F]): Herder[F] = new Herder[F] {

    def run[A](versions: Seq[Version], migration: Migration[File])(f: (Version, MD5) => F[AppliedMigration]): F[ValidatedNec[HerderError, AppliedMigration]] = {
      for {
        existingMigrationOpt <- manager.findByVersionAndName(migration.version, migration.name)
        a <- existingMigrationOpt.map(existingMigration => MigrationsFileValidator.validate(versions, migration, existingMigration)).getOrElse(MigrationsFileValidator.validate(versions, migration))
        b <- a match {
          case Valid((version, md5)) =>
            val value: F[ValidatedNec[HerderError, AppliedMigration]] = existingMigrationOpt match {
              case Some(migration) if migration.status == MigrationStatus.Failed => Sync[F].pure(PreviousMigrationFailed(migration).invalidNec)
              case Some(migration) => Sync[F].pure(migration.validNec[HerderError])
              case None => f(version, md5).map(m => m.validNec[HerderError])
            }
            value
          case Invalid(e) => {
            val value: F[ValidatedNec[HerderError, Nothing]] = Sync[F].pure(Invalid(e))
            value
          }
        }
      } yield {
        b
      }
    }

    override def runUsing(herderConfig: HerderConfig = HerderConfig()): F[Seq[HerderError]] = {
      MigrationsFileValidator.validate(herderConfig) match {
        case Valid(migrations) => {
          val result: F[(List[Version], Seq[Migration[File]])] = migrations.foldLeftM(List.empty[Version], Seq.empty[Migration[File]]) {
            case ((versions, existingMigrations), migration) => {
              run(versions, migration) { (_, md5) =>
                executor
                  .migrate(migration.applyMigration)
                  .handleErrorWith { e =>
                    for {
                      _ <- executor.migrate(migration.unApplyMigration)
                      _ <- manager.add(version = migration.version, name = migration.name, md5 = md5, migrationStatus = MigrationStatus.Failed)
                    } yield {
                      throw e
                    }
                  }.flatMap { _ =>
                  manager.add(version = migration.version, name = migration.name, md5 = md5, migrationStatus = MigrationStatus.Passed)
                }
              }
            }.flatMap {
              case Valid(_) => {
                Sync[F].pure((versions :+ migration.version, existingMigrations :+ migration))
              }
              case Invalid(e) => {
                Sync[F].raiseError[(List[Version], Seq[Migration[File]])](HerderException(e))
              } // error here
            }
          }
          result.map(_ => Seq.empty[HerderError]).handleErrorWith {
            case MigrationFailedException(file, message) =>
              Sync[F].pure(MigrationFailed(file, message) :: Nil)
            case HerderException(herderError) =>
              Sync[F].pure(herderError.toList)
            case (a: IllegalStateException) =>
              Sync[F].pure(NoMigrationsFound("") :: Nil)
          }
        }
        case Invalid(errors) =>
          Sync[F].pure(errors.toList)
      }
    }
  }

}

trait Herder[F[_]] {
  def runUsing(herderConfig: HerderConfig = HerderConfig()): F[Seq[HerderError]]
}


