package com.nycjv321.cats.mongo

import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}

class MigrationStatusCodec extends Codec[MigrationStatus] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext): MigrationStatus = {
    reader.readString() match {
      case "passed" => MigrationStatus.Passed
      case "failed" => MigrationStatus.Failed
    }
  }

  override def encode(writer: BsonWriter, value: MigrationStatus, encoderContext: EncoderContext): Unit = {
    value match {
      case MigrationStatus.Passed => writer.writeString("passed")
      case MigrationStatus.Failed => writer.writeString("failed")
    }
  }

  override def getEncoderClass: Class[MigrationStatus] = classOf[MigrationStatus]
}
