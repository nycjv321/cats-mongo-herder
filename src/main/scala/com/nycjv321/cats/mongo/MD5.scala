package com.nycjv321.cats.mongo

import java.io.{File, InputStream}
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.MessageDigest

import cats.effect.concurrent.Semaphore
import cats.effect.{ContextShift, IO, Resource, Sync}
import cats.implicits._

import scala.concurrent.ExecutionContext

case class MD5(value: String)

object MD5 {
  private implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  def calculate(file: File): MD5= {
    calculateIO(file).unsafeRunSync()
  }

  def calculateF[F[_]: Sync](file: File): F[MD5] = {
    Sync[F].delay {
      calculate(file)
    }
  }

  def calculateIO(file: File): IO[MD5] = {
    def calculateGuarded(guard: Semaphore[IO]): IO[String] = {
      (for {
        i <- inputStream(file, guard)
        g <- digestInputStream(i, guard)
      } yield {
        g
      }).use {
          case (inputStream) =>
            guard.withPermit {
              for {
                b <- generateArrayByte()
                _ <- read(inputStream, b)
              } yield {
                inputStream.
                  getMessageDigest
                  .digest()
                  .map(b => String.format("%02x", b))
                  .mkString("")
              }
        }
      }

    }

    for {
      guard <- Semaphore[IO](1)
      result <- calculateGuarded(guard)
    } yield {
      MD5(result)
    }
  }

  private def read(file: InputStream, buffer: Array[Byte]): IO[String] = {
    for {
      _ <- IO(file.read(buffer))
    } yield {
      new String(buffer, StandardCharsets.UTF_8)
    }
  }

  private def generateArrayByte(): IO[Array[Byte]] = {
    IO {
      new Array[Byte](1024 * 10)
    }
  }


}
