package com.nycjv321.cats.mongo

import java.io.{File, InputStream}
import java.nio.charset.StandardCharsets

import cats.effect.{ContextShift, IO, Sync}

import scala.concurrent.ExecutionContext
import scala.sys.process._


class StringProcessLogger extends ProcessLogger {
  val messages = new StringBuilder

  def lines = messages.toString

  def buffer[T](f: => T): T = {
    messages.clear
    f
  }

  def err(s: => String): Unit = {
    messages.append(s + "\n");
    ()
  }

  def out(s: => String): Unit = {
    messages.append(s + "\n");
    ()
  }
}


trait MigrationExecutor[F[_]] {
  def migrate(applyMigration: File): F[Unit]
}

object MigrationExecutor {
  private implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  def impl[F[_]: Sync](): MigrationExecutor[F] = new MigrationExecutor[F] {

    override def migrate(applyMigration: File): F[Unit] = {

      val processLogger = new StringProcessLogger()
      val status = "mongo" #< applyMigration.getAbsoluteFile ! (processLogger)
      (status, processLogger.lines) match {
        case (code, message) if message.contains("errmsg\" : \"") => {
          Sync[F].raiseError(new MigrationFailedException(applyMigration, message))
        }
        // for now we check the message and error on that
        case (_, _) => Sync[F].pure()
      }
    }


    private def read(file: InputStream, buffer: Array[Byte]): IO[String] = {
      for {
        _ <- IO(file.read(buffer))
      } yield {
        new String(buffer, StandardCharsets.UTF_8)
      }
    }


    //    override def migrate(applyMigration: File): IO[Unit] = {
    //      (for {
    //        guard <- Semaphore[IO](1)
    //        count <- inputStream(applyMigration, guard).use {
    //          case (applyMigrationInputStream) =>
    //            guard.withPermit(migrate(applyMigrationInputStream))
    //        }
    //      } yield count)
    //
    //    }


    private def migrate(file: InputStream): IO[Unit] = {
      for {
        buffer <- IO {
          new Array[Byte](1024 * 10)
        }
        // read migration
        js <- read(file, buffer)
        //        _ <- execute(js)
      } yield {
        ()
      }
    }
  }
}
