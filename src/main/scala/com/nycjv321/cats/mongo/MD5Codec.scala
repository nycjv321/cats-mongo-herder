package com.nycjv321.cats.mongo

import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonWriter}

class MD5Codec extends Codec[MD5] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext): MD5 = MD5(reader.readString())

  override def encode(writer: BsonWriter, value: MD5, encoderContext: EncoderContext): Unit = writer.writeString(value.value)

  override def getEncoderClass: Class[MD5] = classOf[MD5]
}
