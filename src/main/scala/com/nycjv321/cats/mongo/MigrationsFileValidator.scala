package com.nycjv321.cats.mongo

import java.io.File

import cats.SemigroupK
import cats.data.Validated.{Invalid, Valid, _}
import cats.data.{Chain, NonEmptyChain, Validated, ValidatedNec}
import cats.effect.Sync
import cats.implicits._
import com.nycjv321.cats.mongo.VersionsPair.getCurrentAndLastVersion
import cats.data.Validated._
import cats.kernel.Semigroup


object MigrationsFileValidator {
  type ValidationResult[A] = ValidatedNec[HerderError, List[A]]

  private def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(file => file.isFile && file.getName.endsWith(".js")).toList
    } else {
      List[File]()
    }
  }

  def validate(herderConfig: HerderConfig): ValidatedNec[HerderError, List[Migration[File]]] = {
    validateFiles(herderConfig).andThen(validateMigrations(_)).andThen(validateMergedMigrations)
  }

  private def validateFiles(herderConfig: HerderConfig): ValidatedNec[HerderError, List[File]] = {
    val files = getListOfFiles(getClass.getResource(herderConfig.path).getFile)
    if (files.isEmpty) {
      NoMigrationsFound(herderConfig.path).invalidNec
    } else {
      files.validNec
    }
  }

  private def validateMD5[F[_] : Sync](migration: Migration[File], existingMigration: AppliedMigration): F[ValidatedNec[HerderError, MD5]] = {
    for {
      md5 <- MD5.calculateF[F](migration.applyMigration)
    } yield {
      if (existingMigration.md5 == md5) {
        Valid(md5)
      } else {
        InvalidMD5Error(current = md5, previous = existingMigration.md5).invalidNec[MD5]
      }
    }
  }

  private def validateVersions[F[_] : Sync](versions: Seq[Version]) : F[ValidatedNec[HerderError, Version]] = Sync[F].pure {
    VersionsPair.validate(getCurrentAndLastVersion(versions))
  }


  def validate[F[_] : Sync](versions: Seq[Version], migration: Migration[File]): F[ValidatedNec[HerderError, (Int, MD5)]] = {
    for {
      version <- validateVersions(versions)
      md5 <- MD5.calculateF(migration.applyMigration)
    } yield {
      version.map(v => (v, md5))
    }
  }

  def validate[F[_] : Sync](versions: Seq[Version], migration: Migration[File],  existingMigration: AppliedMigration): F[ValidatedNec[HerderError, (Int, MD5)]] = {

    for {
      version <- validateVersions(versions)
      md5 <- MigrationsFileValidator.validateMD5(migration, existingMigration)
    } yield {
      (version,md5).mapN((v, md5) => (v, md5))
    }
  }


  def validateMigrations(files: scala.Seq[File], agg: ValidationResult[PartialMigration[File]] = Valid(List.empty)): ValidationResult[PartialMigration[File]] = {
    files match {
      case head :: tail => {
        (validateMigration(head), validateMigrations(tail)) match {
          case (Valid(a), Valid(b)) => {
            Valid(b :+ a)
          }
          case (Valid(_), Invalid(b)) => {
            Invalid(b)
          }
          case (Invalid(a), Valid(_)) => {
            Invalid(a)
          }
          case (Invalid(a), Invalid(b)) => {
            Invalid(a ++ b)
          }
        }
      }
      case Nil => agg
    }
  }

  def validateMergedMigrations(partialMigrations: scala.List[PartialMigration[File]]): ValidationResult[Migration[File]] = {
    partialMigrations
      .groupBy(migration => (migration.version, migration.name))
      .map(validateCompleteMigration)
      .foldLeft((Chain.empty[HerderError], Seq.empty[Migration[File]])) {
        case ((errors, migrations), Invalid(error: HerderError)) => (errors :+ error, migrations)
        case ((errors, migrations), Invalid(error: Chain[HerderError])) => (errors ++ error, migrations)
        case ((errors, migrations), Valid(migration)) => (errors, migrations :+ migration)
      } match {
      case (errors, Nil) => NonEmptyChain.fromChain(errors).map(Invalid.apply).getOrElse(Valid(List.empty[Migration[File]]))
      case (Chain(), migrations) => Valid(migrations.toList)
    }
  }

  private def validateCompleteMigration(f: ((Int, String), List[PartialMigration[File]])): ValidatedNec[HerderError, Migration[File]] = {
    (f match {
      case ((version, name), PartialMigration(_, _, Some(applyMigration), _) :: PartialMigration(_, _, _, Some(unapplyMigration)) :: Nil) =>
        Right(Migration(version = version, name = name, applyMigration, unapplyMigration))
      case ((version, name), PartialMigration(_, _, _, Some(unapplyMigration)) :: PartialMigration(_, _, Some(applyMigration), _) :: Nil) =>
        Right(Migration(version = version, name = name, applyMigration, unapplyMigration))
      case ((version, name), PartialMigration(_, _, _, None) :: PartialMigration(_, _, Some(_), _) :: Nil) =>
        Left(MissingMigration(version, name, UnapplyMigration))
      case ((version, name), PartialMigration(_, _, _, Some(_)) :: PartialMigration(_, _, None, _) :: Nil) =>
        Left(MissingMigration(version, name, ApplyMigration))
      case ((version, name), migrations) => Left(UnexpectedNumberOfMigrations(version = version, name = name, number = migrations.size))
    }).toValidatedNec
  }

  private def validateVersion(version: String): ValidatedNec[HerderError, Int] = version.toIntOption.toValidNec(UnableToDetermineVersion(version))

  private def validateMigrationMode(fileName: String): ValidatedNec[HerderError, (Int, MigrationMode)] = {
    val modeDelimiterPositionOpt: Option[(Int, MigrationMode)] = if (fileName.endsWith(".apply.js")) {
      Option(fileName.indexOf(".apply.js"), ApplyMigration)
    } else if (fileName.endsWith(".unapply.js")) {
      Option(fileName.indexOf(".unapply.js"), UnapplyMigration)
    } else {
      None
    }
    modeDelimiterPositionOpt.toValidNec(UnableToDetermineMode(fileName))
  }

  private def validateMigration(file: File): ValidatedNec[HerderError, PartialMigration[File]] = {

    val fileName = file.getName
    val versionDelimiterPosition = fileName.indexOf("_")


    (validateVersion(fileName.substring(0, versionDelimiterPosition)), validateMigrationMode(fileName)).mapN {
      case (version: Int, (position: Int, migrationMode: MigrationMode)) =>
        val migrationName = fileName.substring(versionDelimiterPosition + 1, position)
        migrationMode match {
          case ApplyMigration =>
            PartialMigration(version = version, name = migrationName, applyAsOption = Option(file))

          case UnapplyMigration =>
            PartialMigration(version = version, name = migrationName, unApplyAsOption = Option(file))
        }
    }
  }


}
