val Specs2Version = "4.10.0"
val LogbackVersion = "1.2.3"
val CatsEffectVersion = "2.2.0"

testFrameworks += new TestFramework("munit.Framework")

lazy val root = (project in file("."))
  .settings(
    organization := "com.nycjv321",
    name := "cats-mongo-herder",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.2",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % CatsEffectVersion,
      "org.mongodb.scala" %% "mongo-scala-driver" % "2.9.0",
      "org.specs2" %% "specs2-core" % Specs2Version % "test",
      "org.scalameta" %% "munit" % "0.7.21" % Test,
      "org.typelevel" %% "log4cats-core"    % "1.2.0",  // Only if you want to Support Any Backend
      "org.typelevel" %% "log4cats-slf4j"   % "1.2.0"  // Direct Slf4j Support - Recommended
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
  )

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds"
)

