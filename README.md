# (Cats) Mongo Herder
A library for automating mongo migrations within a cats ecosystem

## Setup

1. In your project create a directory for your mongo migrations. 
For example:
    
    ```/src/main/resources/mongo/migrations```
1. This directory will include each migration. Each migration 
should include an _apply_ and _unapply_ version. The _apply_ version
represents the upgrade migration. The accompanying _unapply_ version 
will be used if the apply migration fails. See the section titled 
"Naming" to understand the expected file naming conventions

## Naming

### `{{VERSION}}`
`{{VERSION}}` represents the migration version. For example:
`21_{{NAME}}.apply.js` would mean that you have defined the _21st_ 
migration version. Migrations start at _1_ and then increment from there.

### `{{NAME}}`
`{{NAME}}` represents the migration name. This can be anything but 
we reccomend that it be meaningful and correspond to the operation being 
performed. 
#### Suggested `{{NAME}}` conventions

##### Creating a new document called example
    {{VERSION}}_created_example_document.apply.js
##### adding a new column called foo to a document called example
     {{VERSION}}_add_foo_column_to_example.apply.js
##### Drop column called foo to a document called example
     {{VERSION}}_drop_foo_column_from_example.apply.js
##### adding an unique index to a document called example
     {{VERSION}}_add_unique_index_to_example.apply.js
##### dropping an index from a document called example
     {{VERSION}}_remove_unique_index_from_example.apply.js

 
### Apply Mongo JS
    {{VERSION}}_{{NAME}}.apply.js  
#### UnApply Mongo JS
    {{VERSION}}_{{NAME}}.unapply.js  

## Usage

    import com.nycjv321.cats.mongo.{Herder, HerderConfig, MigrationResults}
    import cats.effect.IO

    ...
    // e.g. F == IO
    private val mongoClient: MongoClient = MongoClient()
    
    val migrations: Migrations[IO] = Migrations.impl[IO](mongoClient)
    val migrationsExecutor: MigrationExecutor[IO] = MigrationExecutor.impl[IO]()

    val herder = Herder.impl[IO](manager = migrations, executor = migrationsExecutor)
    herder.runUsing(HerderConfig(path = path)) // returns sequence of errors


